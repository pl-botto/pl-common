const secrets			= require("./bot_secrets.json");
const config			= require("./config.json");

const Discord			= require('discord.js');
const logger			= require("./logger.js");
const mysql         	= require('mysql2/promise');


const db_table_roles	= "roles"
const db_table_parrot	= "tmp_roles"

const mysq_data = {
    host:       secrets.mysql.host,
    port:       secrets.mysql.port,  
    user:       secrets.mysql.user,
    password:   secrets.mysql.pass,
    database:   secrets.mysql.db
}


const client  = new Discord.Client({ intents: [Discord.GatewayIntentBits.Guilds, Discord.GatewayIntentBits.GuildMembers, Discord.GatewayIntentBits.GuildMessages, Discord.GatewayIntentBits.MessageContent, Discord.GatewayIntentBits.GuildPresences, Discord.GatewayIntentBits.GuildModeration] })
client.login(secrets.discord)


function log_info(module, submodule, func, msg){
	message = `${msg}`.replace(/(\r\n|\n|\r)/gm, " ")
    console.log(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
    logger.info(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
}

function log_warn(module, submodule, func, msg){
	message = `${msg}`.replace(/(\r\n|\n|\r)/gm, " ")
    console.log(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
	logger.warn(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
}

function log_error(module, submodule, func, msg){
	message = `${msg}`.replace(/(\r\n|\n|\r)/gm, " ")
    console.log (`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
    logger.error(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
}



client.on('ready', bot => {
	log_info('PL-Common', 'client.on', 'ready', 'Online')
	
	load_parrot()

	try {

		// Start the member count intervals
		for (var [guild_id, value] of bot.guilds.cache.entries()) {
			if(!config[guild_id]){{return}}
			setInterval(count_members, config[guild_id].module.member_count.interval * 1000, guild_id, bot);
		}
		
	} catch (error){
		log_error('PL-Common', 'client.on', 'ready', error.stack)
	}
});

client.on('warn', warn => {
	log_warn('PL-Common', 'client.on', 'warn', warn.stack)
});

client.on('error', err => {
	log_error('PL-Common', 'client.on', 'error', err.stack)
});
 

// ------------------------------------------------------------------------------------------------------
//
// Muting System
//
// ------------------------------------------------------------------------------------------------------

client.on('messageCreate', async message => {
	try	{
		if(message.author.bot){return}
		if(!config[message.guildId]){{return}}
		if(message.member.roles.cache.has("1191728721359540244") == false && message.member.roles.cache.has(config[message.guildId].common.roles.moderator) == false && message.member.roles.cache.has(config[message.guildId].common.roles.admin) == false){return}

		if(message.content.startsWith(config[message.guildId].module.muted.prefix))
		{
			mute(message)
		}
	} catch (error){
		log_error('PL-Common', 'mute', 'message_create', error.stack)
	}
});

async function mute(message){
	try	{
		let msg = message.content.split(" ")

		// Check Syntax
		if(message.content.length < 24 || msg.length < 2)
		{
			message.reply("Invalid Syntax. Pls use `!mute <UserId>` or `!mute <UserPing>`")
			return;
		}

		// Get Userid 
		if(msg[1].startsWith("<"))
		{
			userid = await msg[1].substring(2, msg[1].length - 1)
		}
		else
		{
			userid = await msg[1]
		}

		// Fetch Member 
		message.guild.members.fetch(userid)
			.then(member => {create_thread(message, member)})
			.catch(err => {
				if(err.message = "Unknown User") { 
					log_warn('PL-Common', 'Mute', 'mute', 'Unknown User')
					message.reply("[Error] Unknown User") 
				}
				else { log_error('PL-Common', 'mute', 'mute', err.stack) }
			});
	} catch (error){
		log_error('PL-Common', 'mute', 'mute', error.stack)
	}
}

// Creates Thead for muted memeber 
async function create_thread(message, member)
{
	try	{
		let channel		= message.guild.channels.cache.get(config[message.guildId].module.muted.channel)
		let thread_opts = Object.create(config[message.guildId].module.muted.threadopts)
		
		thread_opts.name = thread_opts.name + member.user.username

		let threadChannel = await channel.threads.create(thread_opts)
			.then(threadChannel => {return threadChannel})
			.catch((error) => {log_error('PL-Common', 'mute', 'create_thread', error.stack)});

			setTimeout(() => threadChannel.send(`<@${member.id}> you have been muted by the <@&${config[message.guildId].common.roles.moderator}>s. This is the only channel you are allowed to send mesages in.`).catch((error) => {log_error('PL-Common', 'mute', 'create_thread', error.stack)}), 3000);
	} catch (error){
		log_error('PL-Common', 'mute', 'create_thread', error.stack)
	}
}

// ------------------------------------------------------------------------------------------------------
//
// Role System
//
// ------------------------------------------------------------------------------------------------------

client.on('interactionCreate', interaction => {
	try	{

		if (interaction.isCommand()){
			if(interaction.commandName == "send_roles_form")
			{
				if(interaction.member.roles.cache.has(config[interaction.guildId].common.roles.admin) == false){
					interaction.reply({ content:"You do not have the permissions to use this command", ephemeral: true }).catch(error => {
						log_error('PL-Common', '', 'interaction reply', error.stack)
					})                
					return
				}
				send_roles_form(interaction)	
			}

			if(interaction.commandName == "parrot" || interaction.commandName == "unparrot" || interaction.commandName == "parrots"){
				{
					if(interaction.member.roles.cache.has(config[interaction.guildId].common.roles.admin) == false && interaction.member.roles.cache.has(config[interaction.guildId].common.roles.moderator) == false && interaction.member.roles.cache.has("1240302323402539149") == false){
						interaction.reply({ content:"You do not have the permissions to use this command", ephemeral: true }).catch(error => {
							log_error('PL-Common', '', 'interaction reply', error.stack)
						})                
						return
					}
					if(interaction.commandName == "parrot"  	){  parrot(interaction)}
					if(interaction.commandName == "unparrot"	){unparrot(interaction)}
					if(interaction.commandName == "parrots" 	){parrot_list_embed(interaction, {"page": null, "pages": null}, true)}
				}
			}


			if(interaction.commandName == "boomer"){
				{
					if(interaction.member.roles.cache.has(config[interaction.guildId].common.roles.admin) == false && interaction.member.roles.cache.has(config[interaction.guildId].common.roles.moderator) == false){
						interaction.reply({ content:"You do not have the permissions to use this command", ephemeral: true }).catch(error => {
							log_error('PL-Common', '', 'interaction reply', error.stack)
						})                
						return
					}
					if(interaction.commandName == "boomer"){alpha_tester(interaction)}
				}
			}

		} else if (interaction.isStringSelectMenu()){
			if(!interaction.customId == "role_select"){return}
			give_role(interaction)

		} else if (interaction.isButton()){
			if(interaction.customId.startsWith("role_"       )){give_role(interaction)}
			if(interaction.customId.startsWith("parrot_list_")){btn_parrot_list(interaction)}
		}

	} catch (error){
		log_error('PL-Common', 'roles', 'interaction_create', error.stack)
	}
});

async function send_roles_form(interaction){
	try	{

	//  Get  role categories from config
		for (const [name, category] of Object.entries(config[interaction.guildId].module.roles)) {

			if (name == "buttons") // Buttons for roles
			{
				action_row = new Discord.ActionRowBuilder()

				for (let role of category.buttons) {
					action_row.addComponents(
						new Discord.ButtonBuilder()
							.setCustomId(`role_${role.id}`)
							.setLabel(role.name)
							.setStyle(Discord.ButtonStyle.Primary)
							.setEmoji(role.emoji)
					)
				}

				await interaction.channel.send({
					content: category.message,
					components: [action_row],
				});				
			}
			else if (name == "links") // Buttons with links
			{	
				action_row = new Discord.ActionRowBuilder()

				for (let role of category.links) {
					action_row.addComponents(
						new Discord.ButtonBuilder()
							.setURL(role.link)
							.setLabel(role.name)
							.setStyle(Discord.ButtonStyle.Link)
							.setEmoji(role.emoji)					
					)
				}

				await interaction.channel.send({
					content: category.message,
					components: [action_row],
				});	
				
			}
			else // Drop Down Menu
			{
				
				
				let category_array = []
				for (let role of category.roles) {
					menu_option = new Discord.StringSelectMenuOptionBuilder()
					.setLabel(role.name)			
					.setDescription(category.title.replace("%s", role.name))
					.setValue(role.id)

					if(role.emoji != undefined){
						menu_option.setEmoji(role.emoji)
					}

					category_array.push(menu_option)
				}

				action_row = new Discord.ActionRowBuilder().addComponents(
					new Discord.StringSelectMenuBuilder()
					.setCustomId("role_select")
					.setPlaceholder("Nothing selected")
					.setMinValues(0)
					.setMaxValues(category_array.length)
					.addOptions(category_array)
				);

				await interaction.channel.send({
					content: category.message,
					components: [action_row],
				});
			
			}
		}
	} catch (error){
		log_error('PL-Common', 'roles', 'roles_form', error.stack)
	}
}

async function give_role(interaction){
	try	{
		
		if(interaction.isButton()){
			role_id = interaction.customId.split("_")[1]

			let role = await interaction.guild.roles.fetch(role_id);

			if (!interaction.member.roles.cache.has(role.id)) {
				interaction.member.roles.add(role);
				interaction.reply({
				  content: `You got ${role.toString()}`,
				  ephemeral: true,
				});
			} else {
				interaction.member.roles.remove(role);
				interaction.reply({
				  content: `Your role got removed: ${role.toString()}`,
				  ephemeral: true,
				});
			}

		} else {

			let roles_msg = []
			for (let role_id of interaction.values) {
				let role = await interaction.guild.roles.fetch(role_id);

				if (!interaction.member.roles.cache.has(role.id)) {
					interaction.member.roles.add(role);
					roles_msg.push(role.toString())
				} else {
					interaction.member.roles.remove(role);
					roles_msg.push(role.toString())
				}		
			}

			interaction.reply({
				content: `Your roles got added/removed: ${roles_msg}`,
				ephemeral: true,
			});
		}

	} catch (error){
		log_error('PL-Common', 'roles', 'give_role', error.stack)
	}
}


// ------------------------------------------------------------------------------------------------------
//
// Member Counting System
//
// ------------------------------------------------------------------------------------------------------

function numberWithCommas(x) {
	return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
}


function count_members(guild_id, bot) {
	try	{
		let  guild	= bot  .guilds  .cache.get(guild_id);
		let  chann	= guild.channels.cache.get(config[guild_id].module.member_count.channel);
		let  bots	= guild.roles   .cache.get(config[guild_id].common.roles.bots)
		
		let  count	= numberWithCommas(guild.memberCount - bots.members.size)


		chann.setName(count + config[guild_id].module.member_count.message)
	} catch (error){
		log_error('PL-Common', 'count_members', 'count_members', error.stack)
	}
}

// ------------------------------------------------------------------------------------------------------
//
// Alpha Tester
//
// ------------------------------------------------------------------------------------------------------

async function alpha_tester(interaction){
	try {
		let role_id = "1274822961874075689"
		let usr		= interaction.options.getUser   ('user')

		let	member	= await interaction.guild.members.fetch(usr.id)

		if (!member.roles.cache.has(role_id)) {
			member.roles.add(role_id);
			interaction.reply({ content:`Alpha tester role was succesfully added`, ephemeral: true }).catch(error => {log_error('PL-Common', 'alpha_tester', 'interaction reply', error.stack)})
		} else {
			interaction.reply({ content:`User ${user_id} already has ALpha Tester Role`, ephemeral: true }).catch(error => {log_error('PL-Common', 'alpha_tester', 'interaction reply', error.stack)})
			return
		}
    } catch (error){
        log_error('PL-Common', 'alpha tester', 'alpha tester', error.stack)
    }
}

// ------------------------------------------------------------------------------------------------------
//
// Caged Parrot
//
// ------------------------------------------------------------------------------------------------------

async function parrot(interaction){
	try {
		let dur	= 0
		let usr	= interaction.options.getUser   ('user')
		let hou	= interaction.options.getInteger('hours')
		let min	= interaction.options.getInteger('minutes')
		let sec	= interaction.options.getInteger('seconds')

		let	member		= await interaction.guild.members.fetch(usr.id)
		
		if(hou){dur = dur + hou * 3600}
		if(min){dur = dur + min * 60  }
		if(sec){dur = dur + sec * 1   }
		
		let role_id		= null
		let guild_id	= interaction.guildId
		let user_id		= usr.id
		let tsp			= Math.trunc(Date.now()/1000) + dur

		if(dur == 0){ tsp =  0} // Indefenite default value

		var mysql_conn	= await  mysql.createConnection(mysq_data);


		const [results_rid, fields_rid] = await mysql_conn.execute('SELECT * FROM ' + db_table_roles + ' WHERE role_name = ? and guild_id = ?', ["parrot", guild_id])

		if(results_rid.length == 1){
			role_id = results_rid[0]["role_id"]
		}


		if(role_id){

			const [results_exsist, fields_exsist] = await mysql_conn.execute('SELECT * FROM ' + db_table_parrot + ' WHERE user_id = ? and guild_id = ? and role_id = ?', [user_id, guild_id, role_id])

			if(results_exsist.length > 0){
				interaction.reply({ content:`User ${user_id} already has Parrot Role`, ephemeral: true }).catch(error => {log_error('PL-Common', 'parrot', 'interaction reply', error.stack)})
				return
			}

			if (!member.roles.cache.has(role_id)) {
				member.roles.add(role_id);
			} else {
				interaction.reply({ content:`User ${user_id} already has Parrot Role`, ephemeral: true }).catch(error => {log_error('PL-Common', 'parrot', 'interaction reply', error.stack)})
				return
			}

			
			
			if(dur != 0){
				log_info('PL-Common', 'parrot', 'remove parrot', `Added Parrot role to user ${user_id} on guild ${guild_id} until ${tsp}.`)
				setTimeout(() => {remove_parrot(guild_id, user_id, role_id)}, 1000 * dur);
			} else {
				log_info('PL-Common', 'parrot', 'remove parrot', `Added Parrot role to user ${user_id} on guild ${guild_id} for ever.`)
			}



			const [results_insert, fields_insert] = await mysql_conn.execute('INSERT INTO ' + db_table_parrot + ' (guild_id, role_id, user_id, tsp) VALUES (?,?,?,?)', [guild_id, role_id, user_id, tsp])

			if(results_insert["affectedRows"] == 1){
				if (dur != 0){
					interaction.reply({ content:`Parrot role was succesfully added to user until <t:${tsp}>.`, ephemeral: true }).catch(error => {log_error('PL-Common', 'parrot', 'interaction reply', error.stack)})
				} else {
					interaction.reply({ content:`Parrot role was succesfully added to user for ever.`, ephemeral: true }).catch(error => {log_error('PL-Common', 'parrot', 'interaction reply', error.stack)})
				}
			} else {
				interaction.reply({ content:`Adding Parrot role to user failed.`, ephemeral: true }).catch(error => {log_error('PL-Common', 'parrot', 'interaction reply', error.stack)})
			}

		} else {
			interaction.reply({ content:"Error RoleId for Parrot Role is not configured.", ephemeral: true }).catch(error => {log_error('PL-Common', 'parrot', 'interaction reply', error.stack)})
		}

    } catch (error){
        log_error('PL-Common', 'parrot', 'parrot', error.stack)
    }
}

async function unparrot(interaction){
	try {
		let usr			= interaction.options.getUser('user')
		let	member		= await interaction.guild.members.fetch(usr.id)

		let role_id		= null
		let guild_id	= interaction.guildId
		let user_id		= usr.id

		var mysql_conn	= await  mysql.createConnection(mysq_data);

		const [results_rid, fields_rid] = await mysql_conn.execute('SELECT * FROM ' + db_table_roles + ' WHERE role_name = ? and guild_id = ?', ["parrot", guild_id])

		if(results_rid.length == 1){
			role_id = results_rid[0]["role_id"]
		}

		if(role_id){

			const [results_del, fields_del] = await mysql_conn.execute('DELETE FROM ' + db_table_parrot + ' WHERE user_id = ? and guild_id = ? and role_id = ?', [user_id, guild_id,role_id])

			if (member.roles.cache.has(role_id)) {
				member.roles.remove(role_id);

				if(results_del["affectedRows"] == 1){
					interaction.reply({ content:`Parrot role was succesfully removed from user.`, ephemeral: true }).catch(error => {log_error('PL-Common', 'parrot', 'interaction reply', error.stack)})
				} else {
					interaction.reply({ content:`Removing Parrot role to user failed.`, ephemeral: true }).catch(error => {log_error('PL-Common', 'parrot', 'interaction reply', error.stack)})
				}
			
			} else {
				interaction.reply({ content:`User ${user_id} does not have Parrot Role`, ephemeral: true }).catch(error => {log_error('PL-Common', 'parrot', 'interaction reply', error.stack)})
				return
			}

		} else {
			interaction.reply({ content:"Error RoleId for Parrot Role is not configured.", ephemeral: true }).catch(error => {log_error('PL-Common', 'parrot', 'interaction reply', error.stack)})
		}

    } catch (error){
        log_error('PL-Common', 'parrot', 'unparrot', error.stack)
    }

}

async function remove_parrot(guild_id, user_id, role_id){
	try {

		let  guild	= client.guilds.cache.get(guild_id)
		let  member	= await guild.members.fetch(user_id)

		var mysql_conn	= await  mysql.createConnection(mysq_data);

		member.roles.remove(role_id)
		
		const [results_del, fields_del] = await mysql_conn.execute('DELETE FROM ' + db_table_parrot + ' WHERE user_id = ? and guild_id = ? and role_id = ?', [user_id, guild_id,role_id])

		if(results_del["affectedRows"] == 0){
			log_error('PL-Common', 'parrot', 'remove parrot', `Removing Parrot role from user ${user_id} on guild ${guild_id} failed.`)
		} else {
			log_info('PL-Common', 'parrot', 'remove parrot', `Removed Parrot role from user ${user_id} on guild ${guild_id}.`)
		}
    } catch (error){
        log_error('PL-Common', 'parrot', 'remove parrot', error.stack)
    }
}

async function load_parrot(){
	try {

		let tsp_now		= Math.trunc(Date.now()/1000)
		var mysql_conn	= await  mysql.createConnection(mysq_data);


		const [results_rids, fields_rid] = await mysql_conn.execute('SELECT guild_id, role_id FROM ' + db_table_roles + ' WHERE role_name = ?', ["parrot"])

		for(var result of results_rids){
			let guild_id = result["guild_id"]
			let role_id  = result["role_id" ]

			var [results_load, _] = await mysql_conn.execute('SELECT user_id, tsp FROM ' + db_table_parrot + ' WHERE guild_id = ? and role_id = ?', [guild_id, role_id])
		
			for(var result of results_load){
				user_id  = result["user_id"]
				tsp_diff = result["tsp"] - tsp_now

				if(result["tsp"] == 0){continue}

				if(tsp_diff > 0){
					setTimeout(() => {remove_parrot(guild_id, user_id, role_id)}, 1000 * tsp_diff)
				} else {
					remove_parrot(guild_id, user_id, role_id)
				}
			}
		}
    } catch (error){
        log_error('PL-Common', 'parrot', 'load parrot', error.stack)
    }
}


async function parrot_list_embed(interaction, args, reply){
	/*
	┌──────────────────────────────┐
	│ Caged Parrots                │
	│ Page: 1/3                    │
	│ User     Role    Expiration  │
	│ User_10  <role>   <t:tsp>    │
	│ User_9   <role>   <t:tsp>    │
	│ User_8   <role>   <t:tsp>    │
	│ User_7   <role>   <t:tsp>    │
	│ User_6   <role>   <t:tsp>    │
	│ User_5   <role>   <t:tsp>    │
	│ User_4   <role>   <t:tsp>    │
	│ User_3   <role>   <t:tsp>    │
	│ User_2   <role>   <t:tsp>    │
	│ User_1   <role>   <t:tsp>    │
	│  [1|1]                       │
	└──────────────────────────────┘

	┌───┐┌───┐┌───┐┌───┐┌───┐
	│ « ││ < ││ _ ││ > ││ » │
	└───┘└───┘└───┘└───┘└───┘
	└ Page Change  
	*/

	try {
		if(args.page	== null){args.page	= 1}
		if(args.pages	== null){args.pages	= 1}
		let col1 	= ""
		let col2 	= ""
		let col3 	= ""

		var mysql_conn = await  mysql.createConnection(mysq_data);

		const [results_sys, fields] = await mysql_conn.execute('SELECT COUNT(*) FROM ' + db_table_parrot + ' WHERE guild_id = ?', [interaction.guildId])


		if(results_sys[0]['COUNT(*)'] != 0)
		{
			args.pages = Math.ceil(results_sys[0]['COUNT(*)'] / 10).toFixed();

			const [results, fields] = await mysql_conn.execute('SELECT * FROM ' + db_table_parrot + ' WHERE guild_id = ? LIMIT 10 OFFSET '+ (args.page - 1)* 10, [interaction.guildId])

			if(results.length < 1){
				interaction.reply({ content:"No User has the Caged Parrot Role", ephemeral: true }).catch(error => {log_error('PL-Common', 'parrot list', 'interaction reply', error.stack)}) 
				return null
			}


			for (let i = 0; i < results.length && i < 10; i++) {	
				col1 = col1 + `<@${ results[i]["user_id"]}>\n`
				col2 = col2 + `<@&${results[i]["role_id"]}>\n`

				if(results[i]["tsp"] == 0){
					col3 = col3 + `never\n`
				} else {
					col3 = col3 + `<t:${results[i]["tsp"]}>\n`
				}
			}

			contest_sys_first	= new Discord.ButtonBuilder().setCustomId('parrot_list_first').setLabel('<<').setStyle(Discord.ButtonStyle.Primary)
			contest_sys_prev	= new Discord.ButtonBuilder().setCustomId('parrot_list_prev' ).setLabel('<') .setStyle(Discord.ButtonStyle.Primary)
			contest_sys_mid		= new Discord.ButtonBuilder().setCustomId('parrot_list_mid'  ).setLabel('─') .setStyle(Discord.ButtonStyle.Primary)
			contest_sys_next	= new Discord.ButtonBuilder().setCustomId('parrot_list_next' ).setLabel('>') .setStyle(Discord.ButtonStyle.Primary)
			contest_sys_last	= new Discord.ButtonBuilder().setCustomId('parrot_list_last' ).setLabel('>>').setStyle(Discord.ButtonStyle.Primary)

			let contest_sys_embed = new  Discord.EmbedBuilder()
			.setTitle("Caged Parrots")
			.setColor(config[interaction.guildId].common.embed)
			.setDescription(`Page: ${args.page}/${args.pages}`)
			.setFooter({ text: `[${args.page}|${args.pages}]`})
			.addFields(
				{name:'User',		value: col1, inline: true},
				{name:'Role',		value: col2, inline: true},
				{name:'Expiration',	value: col3, inline: true},
			)
			
			if(reply) // Reply with embed 
				interaction.reply({embeds: [contest_sys_embed], components: [{type: 1,  components:[contest_sys_first,contest_sys_prev,contest_sys_mid,contest_sys_next,contest_sys_last]}]}).catch(error => {
					log_error('PL-Common', 'parrot list', 'interaction reply', error.stack)
				})

				
			else // Update embed 
			{
				interaction.update({embeds: [contest_sys_embed], components: [{type: 1,  components:[contest_sys_first,contest_sys_prev,contest_sys_mid,contest_sys_next,contest_sys_last]}]}).catch(error => {
					log_error('PL-Common', 'parrot list', 'interaction update', error.stack)
				})
			}			

		} else {
			interaction.reply({ content:"No User has the Caged Parrot Role", ephemeral: true }).catch(error => {log_error('PL-Common', 'parrot list', 'interaction reply', error.stack)}) 
			return null
		}


		mysql_conn.end()

	} catch (error){
		log_error('PL-Common', 'parrot list', 'parrot list embed', error.stack)
		return null
	}
}

function btn_parrot_list (interaction){
	try {
		let button	= interaction.customId.split("_")
		let footer	= interaction.message.embeds[0].footer.text.replace("[","").replace("]","").split("|")


		if(button.length != 2 && footer.length != 2){
			interaction.reply({ content:"Unsupported Command", ephemeral: true }).catch(error => {
				log_error('PL-Common', 'parrots list button', 'interaction reply', error.stack)
			})  
			return
		}

		args = {
			"page"	: 	parseInt(footer[0]),
			"pages"	:	parseInt(footer[1]),
		}

		if(button[2] == "first"){
			if(args.page > 1){args.page = 1}
			else
			{
				interaction.reply({content:"Already on first page", ephemeral: true }).catch(error => {log_error('PL-Common', 'parrots list button', 'interaction reply', error.stack)})
				return
			}
		}
		else if(button[2] == "next")
		{
			if(args.page < args.pages){args.page = args.page + 1}
			else
			{
				interaction.reply({content:"Already on last page", ephemeral: true }).catch(error => {log_error('PL-Common', 'parrots list button', 'interaction reply', error.stack)})
				return
			}
		}
		else if(button[2] == "mid")
		{
			interaction.reply({ content:"This button is just for alignment.", ephemeral: true }).catch(error => {log_error('PL-Common', 'parrots list button', 'interaction reply', error.stack)})
			return
		}
		else if(button[2] == "prev")
		{
			if(args.page > 1){args.page = args.page - 1}
			else
			{
				interaction.reply({content:"Already on first page", ephemeral: true }).catch(error => {log_error('PL-Common', 'parrots list button', 'interaction reply', error.stack)})
				return
			}
		}
		else if(button[2] == "last")
		{
			if(args.page != args.pages){args.page = args.pages}
			else
			{
				interaction.reply({content:"Already on last page", ephemeral: true }).catch(error => {log_error('PL-Common', 'parrots list button', 'interaction reply', error.stack)})
				return
			}
		}

		if(button[2] == "first" || button[2] == "next" || button[2] == "prev" || button[2] == "last" || button[2] == "back")
		{
			embed = parrot_list_embed(interaction, args, false)
		}

	} catch (error){
		log_error('PL-Common', 'parrots list button', 'parrots list button', error.stack)
	}
}

// ------------------------------------------------------------------------------------------------------
//
// Ban Sync
//
// ------------------------------------------------------------------------------------------------------

client.on('guildBanAdd', async ban => {
	try {
		let ban_source	= await ban.guild.id
		let user_id		= await ban.user.id
		let bot			= await ban.client

		

		for (var [guild_id, guild] of bot.guilds.cache.entries()) {
			if(String(guild_id) !== "728905379274162177" && String(guild_id) !== "839672543140904971"){continue}
			if(String(guild_id) !== String(ban.guild.id)){
				let ban_src = ban.guild.name
				let guild	= await bot.guilds   .cache.get(guild_id);

				guild.members.ban(user_id, { deleteMessageSeconds: 60 * 60 * 24 * 7, reason: `Ban Sync from ${ban_src}`})
				.then(info   => {log_info('PL-Common', 'Ban Sync', 'Ban', `Banned user: ${user_id} in ${guild}`)})
				.catch(error => {log_error('PL-Common', 'Ban Sync', 'Ban', `Guild: ${guild} Error: ${error.stack}`)});
			}	
		}	
	} catch (error){
		log_error('PL-Common', 'Ban Sync', 'Ban', error.stack)
	}
});

client.on('guildBanRemove', async ban => {
	try {
		let ban_source	= await ban.guild.id
		let user_id		= await ban.user.id
		let bot			= await ban.client

		for (var [guild_id, guild] of bot.guilds.cache.entries()) {
			if(String(guild_id) !== "728905379274162177" && String(guild_id) !== "839672543140904971"){continue}
			if(String(guild_id) !== String(ban_source)){
				let guild	= await bot.guilds   .cache.get(guild_id);
				
				guild.members.unban(user_id)
				.then(log_info('PL-Common', 'Ban Sync', 'Unban', `Unbanned user: ${user_id} in ${guild}`))
				.catch(error => {log_error('PL-Common', 'Ban Sync', 'Unban', `Guild: ${guild} Error: ${error.stack}`)});
			}	
		}	
	} catch (error){
		log_error('PL-Common', 'Ban Sync', 'Unban', error.stack)
	}
});
