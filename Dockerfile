FROM alpine:latest

RUN apk update
RUN apk add nodejs
RUN apk add npm 
RUN node -v
COPY ./discord-bot /discord-bot

WORKDIR "/discord-bot"
RUN npm install --no-bin-links
